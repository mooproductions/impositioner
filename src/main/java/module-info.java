module Impositioner.main {
    requires java.base;
    requires java.desktop;
    requires javafx.controls;
    requires javafx.fxml;
    requires javafx.graphics;
    requires org.apache.pdfbox;
    opens productions.moo;
}