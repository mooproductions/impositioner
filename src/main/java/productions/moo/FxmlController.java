package productions.moo;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.FlowPane;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;
import javafx.stage.Window;
import javafx.stage.WindowEvent;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;

public class FxmlController implements Initializable, EventHandler<WindowEvent> {
    private final BookManager bookManager = new BookManager();
    @FXML
    private FlowPane previewGrid;
    @FXML
    private ContextMenu contextMenu;
    @FXML
    private Slider scale;
    @FXML
    private Text pageCount;
    @FXML
    private Text signatureCount;
    @FXML
    private Text blankPageCount;
    @FXML
    private ComboBox<PaperSize> paperSize;
    @FXML
    private ComboBox<PaperSize> pageSize;
    @FXML
    private ComboBox<Integer> pagesPerSignature;
    private File bookFile = null;
    private File impositionFile = null;
    private Node selectedPage = null;
    private boolean isDirty = false;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        paperSize.setItems(PaperSize.getSizes());
        pageSize.setItems(PaperSize.getSizes());
        paperSize.setValue(PaperSize.A4);
        setPaperSize(PaperSize.A4);
    }

    @FXML
    private void handleOpen(final ActionEvent event) {
        if (isDirty) {
            Optional<ButtonType> choice = showConfirmationDialog();
            if (choice.isEmpty() || choice.get() == ButtonType.CANCEL) {
                return;
            }
        }

        Window window = ((MenuItem) event.getTarget()).getParentPopup().getOwnerWindow();
        FileChooser fileChooser = buildChooser("Open Book");
        bookFile = fileChooser.showOpenDialog(window);

        if (bookFile == null) return;

        Task<Void> loadBook = new Task<>() {
            @Override
            protected Void call() throws Exception {
                bookManager.loadBook(bookFile);
                return null;
            }
        };

        Alert dialog = new Alert(Alert.AlertType.NONE);
        dialog.setResult(ButtonType.OK);
        dialog.setTitle("Please Wait");
        dialog.setHeaderText(null);
        dialog.setContentText("Loading Book...");
        dialog.show();

        loadBook.setOnSucceeded(e -> {
            isDirty = false;
            scale.setValue(1.0);
            adjustScale(1.0);
            adjustImposition();
            dialog.close();
        });

        loadBook.setOnFailed(e -> {
            dialog.close();
        });

        new Thread(loadBook).start();
    }

    @FXML
    private void handleSave(final ActionEvent event) {
        saveBook();
    }

    @FXML
    private void handleSaveAs(final ActionEvent event) {
        Window window = ((MenuItem) event.getTarget()).getParentPopup().getOwnerWindow();
        FileChooser fileChooser = buildChooser("Save As...");
        bookFile = fileChooser.showSaveDialog(window);
        saveBook();
    }

    private void saveBook() {
        try {
            bookManager.saveBook(bookFile);
            isDirty = false;
        } catch (IOException e) {
            handleException(e, "Failed to save book.");
        }
    }

    private void handleException(Exception e, String message) {
        e.printStackTrace();
        Alert dialog = new Alert(Alert.AlertType.ERROR);
        dialog.setTitle(message);
        dialog.setHeaderText(null);
        dialog.setContentText(message + "\n" + e.getMessage());
        dialog.show();
    }

    @FXML
    private void handleExport(final ActionEvent event) {
        if (impositionFile == null) {
            handleExportAs(event);
        } else {
            exportBook();
        }
    }

    @FXML
    private void handleExportAs(final ActionEvent event) {
        Window window = ((MenuItem) event.getTarget()).getParentPopup().getOwnerWindow();
        FileChooser fileChooser = buildChooser("Save As...");
        impositionFile = fileChooser.showSaveDialog(window);
        exportBook();
    }

    private void exportBook() {
        try {
            bookManager.impositionBook(paperSize.getValue(), pageSize.getValue(), pagesPerSignature.getValue(), impositionFile);
        } catch (Exception e) {
            handleException(e, "Failed to export book.");
        }
    }

    @FXML
    private void handleExit() {
        if (shouldClose()) {
            Platform.exit();
            System.exit(0);
        }
    }

    @Override
    public void handle(WindowEvent event) {
        if (event.getEventType() == WindowEvent.WINDOW_CLOSE_REQUEST && !shouldClose()) {
            event.consume();
        }
    }

    private boolean shouldClose() {
        if (isDirty) {
            Optional<ButtonType> button = showConfirmationDialog();
            return button.isPresent() && button.get() == ButtonType.OK;
        } else {
            return true;
        }
    }

    @FXML
    private void handleChangePaperSize(ActionEvent event) {
        setPaperSize(paperSize.getValue());
    }

    @FXML
    private void handleChangePageSize(ActionEvent event) {
        setPageSize(pageSize.getValue());
    }

    @FXML
    private void handleChangePagesPerSignature(ActionEvent event) {
        adjustImposition();
    }

    @FXML
    private void handleAdjustScale(MouseEvent event) {
        adjustScale(scale.getValue());
    }

    @FXML
    private void handleDeletePage(ActionEvent event) {
        bookManager.deletePage(previewGrid.getChildren().indexOf(selectedPage));
        isDirty = true;
        selectedPage = null;
        adjustScale(scale.getValue());
        adjustImposition();
    }

    @FXML
    private void handleInsertPageBefore(ActionEvent event) {
        insertPage(previewGrid.getChildren().indexOf(selectedPage));
    }

    @FXML
    private void handleInsertPageAfter(ActionEvent event) {
        insertPage(previewGrid.getChildren().indexOf(selectedPage) + 1);
    }

    @FXML
    private void handleDuplicatePage(ActionEvent event) {
        TextInputDialog dialog = new TextInputDialog("1");
        dialog.setTitle("Duplicate Page");
        dialog.setHeaderText(null);
        dialog.setContentText("How many times should this page be duplicated?");
        dialog.showAndWait().ifPresent((input) -> {
            try {
                int number = Integer.parseInt(input);
                duplicatePage(previewGrid.getChildren().indexOf(selectedPage), number);
                isDirty = true;
            } catch (NumberFormatException e) {
                handleException(e, "Failed to duplicate page.");
            }
        });
    }

    private void insertPage(int index) {
        try {
            bookManager.insertPage(index);
            isDirty = true;
            selectedPage = null;
            adjustScale(scale.getValue());
            adjustImposition();
        } catch (IOException e) {
            handleException(e, "Failed to insert page.");
        }
    }

    private void duplicatePage(int index, int times) {
        try {
            bookManager.duplicatePage(index, times);
            isDirty = true;
            selectedPage = null;
            adjustScale(scale.getValue());
            adjustImposition();
        } catch (IOException e) {
            handleException(e, "Failed to duplicate page.");
        }
    }

    private void adjustScale(double scale) {
        List<Node> children = previewGrid.getChildren();
        if (children.size() > 0) {
            previewGrid.getChildren().removeAll(children);
        }
        previewGrid.getChildren().addAll(bookManager.getPreviews(scale));
        for (Node page : previewGrid.getChildren()) {
            page.setOnContextMenuRequested(event -> {
                contextMenu.show(page, event.getScreenX(), event.getScreenY());
                selectedPage = page;
            });
        }
    }

    private void setPaperSize(PaperSize paper) {
        PaperSize page = pageSize.getValue();
        if (page == null || page.ordinal() <= paper.ordinal()) {
            page = PaperSize.values()[paper.ordinal() + 1];
            pageSize.setValue(page);
        }

        setPageSize(page);
    }

    private void setPageSize(PaperSize page) {
        PaperSize paper = paperSize.getValue();

        Integer signatureSize = this.pagesPerSignature.getValue();

        int pagesPerSheet = PaperSize.pagesPerSheet(paper, page);
        int maxSignatureSize = 32;

        if (pagesPerSheet < 2) {
            return;
        }

        List<Integer> possibleSignatureSizes = new ArrayList<>();
        for (int i = pagesPerSheet; i <= maxSignatureSize; i += pagesPerSheet) {
            possibleSignatureSizes.add(i);
        }

        if (signatureSize == null || !possibleSignatureSizes.contains(signatureSize)) {
            signatureSize = possibleSignatureSizes.get((int) ((possibleSignatureSizes.size() - 0.5) / 2));
        }

        pagesPerSignature.setItems(FXCollections.observableArrayList(possibleSignatureSizes));
        pagesPerSignature.setValue(signatureSize);

        adjustImposition();
    }

    private void adjustImposition() {
        int pages = previewGrid.getChildren().size();
        Integer signatureSize = pagesPerSignature.getValue();

        if (signatureSize == null) return;

        int signatures = (int) Math.ceil(pages / (double) signatureSize);
        int blankPages = signatureSize - (pages % signatureSize);
        pageCount.setText(Integer.toString(pages));
        signatureCount.setText(Integer.toString(signatures));
        blankPageCount.setText(Integer.toString(blankPages));
    }

    private FileChooser buildChooser(String title) {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle(title);
        fileChooser
                .getExtensionFilters()
                .addAll(new FileChooser.ExtensionFilter("PDF Files", "*.pdf"));

        return fileChooser;
    }

    private Optional<ButtonType> showConfirmationDialog() {
        Alert dialog = new Alert(Alert.AlertType.CONFIRMATION);
        dialog.setTitle("Unsaved Changes.");
        dialog.setHeaderText(null);
        dialog.setContentText("There are unsaved changes, are you sure you want to quit?");
        return dialog.showAndWait();
    }
}
