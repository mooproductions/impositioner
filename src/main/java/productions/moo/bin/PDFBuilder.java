package productions.moo.bin;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.apache.pdfbox.pdmodel.font.PDType1Font;

import java.io.IOException;

public class PDFBuilder {
    public static void main(String[] args) {
        PDDocument doc = new PDDocument();

        for (int i = 1; i <= 32; i++) {
            try {
                PDPage page = new PDPage(PDRectangle.A4);
                doc.addPage(page);
                PDPageContentStream content = new PDPageContentStream(doc, page);

                // Draw Page Numbers
                content.beginText();
                content.setFont(PDType1Font.HELVETICA, 40);
                content.newLineAtOffset(page.getCropBox().getWidth() / 2, page.getCropBox().getHeight() / 2);
                content.showText(Integer.toString(i));
                content.endText();

                // Draw Box
                content.setLineWidth(3);
                content.moveTo(10, 10);
                content.lineTo(page.getCropBox().getWidth() - 10, 10);
                content.lineTo(page.getCropBox().getWidth() - 10, page.getCropBox().getHeight() - 10);
                content.lineTo(10, page.getCropBox().getHeight() - 10);
                content.lineTo(10, 10);
                // This last part is here to make sure the corner looks nice.
                content.lineTo(page.getCropBox().getWidth() - 10, 10);
                content.stroke();
                content.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        try {
            doc.save("Test.pdf");
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                doc.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
