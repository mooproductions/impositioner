package productions.moo;

import javafx.collections.FXCollections;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.image.PixelWriter;
import javafx.scene.image.WritableImage;
import org.apache.pdfbox.cos.COSDictionary;
import org.apache.pdfbox.cos.COSName;
import org.apache.pdfbox.io.IOUtils;
import org.apache.pdfbox.io.RandomAccessBufferedFileInputStream;
import org.apache.pdfbox.io.RandomAccessRead;
import org.apache.pdfbox.pdfparser.PDFParser;
import org.apache.pdfbox.pdmodel.*;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.apache.pdfbox.pdmodel.graphics.form.PDFormXObject;
import org.apache.pdfbox.rendering.PDFRenderer;

import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class BookManager {
    private final List<Image> pagePreviews = new ArrayList<>();
    private final List<PDDocument> intermediates = new ArrayList<>();
    private PDDocument originalDoc = null;

    public void loadBook(File file) throws IOException {
        if (originalDoc != null) {
            originalDoc.close();
            originalDoc = null;
        }

        RandomAccessRead raFile = new RandomAccessBufferedFileInputStream(new FileInputStream(file));

        PDFParser parser = new PDFParser(raFile);
        parser.parse();

        originalDoc = parser.getPDDocument();
        PDFRenderer renderer = new PDFRenderer(originalDoc);
        pagePreviews.clear();
        for (int i = 0; i < originalDoc.getNumberOfPages(); i++) {
            Image image = convertToFxImage(renderer.renderImage(i));
            pagePreviews.add(image);
        }

        FXCollections.observableArrayList(PaperSize.values());
    }

    public void saveBook(File file) throws IOException {
        originalDoc.save(file);
    }

    private PDDocument scaleDoc(PaperSize targetSize, PDDocument doc) throws IOException {
        PDRectangle targetPageSize = targetSize.getRect();
        PDDocument scaledDoc = new PDDocument();
        intermediates.add(scaledDoc);

        for (int i = 0; i < doc.getNumberOfPages(); i++) {
            PDPage page = doc.getPage(i);
            PDPage scaledPage = new PDPage(targetPageSize);

            double scale = Math.min(targetPageSize.getWidth() / page.getCropBox().getWidth(), targetPageSize.getHeight() / page.getCropBox().getHeight());
            double xOffset = (targetPageSize.getWidth() - page.getCropBox().getWidth() * scale) / 2;
            double yOffset = (targetPageSize.getHeight() - page.getCropBox().getHeight() * scale) / 2;

            scaledPage.setResources(new PDResources());
            scaledDoc.addPage(scaledPage);

            PDFormXObject xObject = importAsXObject(scaledDoc, page);
            scaledPage.getResources().add(xObject);

            PDPageContentStream content = new PDPageContentStream(scaledDoc, scaledPage);
            AffineTransform transform = new AffineTransform(scale, 0, 0, scale, xOffset, yOffset);
            xObject.setMatrix(transform);
            content.drawForm(xObject);
            content.close();
        }

        return scaledDoc;
    }

    public void impositionBook(PaperSize paperSize, PaperSize pageSize, int pagesPerSignature, File file) throws IOException, UnsupportedOperationException {
        int signatures = (int) Math.ceil(originalDoc.getNumberOfPages() / (double) pagesPerSignature);

        int folds = PaperSize.foldsBetween(paperSize, pageSize);
        PDDocument impositionedDoc = switch (folds) {
            case 1 -> twoUpMode(paperSize, pageSize, signatures, pagesPerSignature);
            case 2 -> fourUpMode(paperSize, pageSize, signatures, pagesPerSignature);
            case 3 -> eightUpMode(paperSize, pageSize, signatures, pagesPerSignature);
            default -> throw new UnsupportedOperationException("Only impositions with three or fewer folds are supported.");
        };

        impositionedDoc.save(file);

        for (PDDocument doc : intermediates) {
            doc.close();
        }

        intermediates.clear();
    }

    private PDDocument twoUpMode(PaperSize paperSize, PaperSize pageSize, int signatures, int pagesPerSignature) throws IOException {
        PDRectangle targetPaperSize = paperSize.getRect();
        PDRectangle targetPageSize = pageSize.getRect();

        PDDocument scaledDoc = scaleDoc(pageSize, originalDoc);
        PDDocument impositionedDoc = new PDDocument();
        intermediates.add(impositionedDoc);

        final double theta = Math.PI / 2;
        for (int signature = 0; signature < signatures; signature++) {
            for (int offset = 0; offset < pagesPerSignature / 2; offset++) {
                PDPage twoUp = new PDPage(targetPaperSize);
                twoUp.setRotation(90);
                twoUp.setResources(new PDResources());
                impositionedDoc.addPage(twoUp);

                int leftIndex = pagesPerSignature * signature + pagesPerSignature - offset - 1;
                int rightIndex = pagesPerSignature * signature + offset;

                PDPage leftPage = getPage(scaledDoc, leftIndex, targetPageSize);
                PDPage rightPage = getPage(scaledDoc, rightIndex, targetPageSize);

                PDFormXObject leftObject = importAsXObject(impositionedDoc, leftPage);
                twoUp.getResources().add(leftObject);

                PDFormXObject rightObject = importAsXObject(impositionedDoc, rightPage);
                twoUp.getResources().add(rightObject);

                PDPageContentStream content = new PDPageContentStream(impositionedDoc, twoUp);

                AffineTransform leftTrans = AffineTransform.getRotateInstance(theta);
                leftTrans.concatenate(AffineTransform.getTranslateInstance(0, -targetPageSize.getHeight()));

                AffineTransform rightTrans = AffineTransform.getRotateInstance(theta);
                rightTrans.concatenate(AffineTransform.getTranslateInstance(targetPageSize.getWidth(), -targetPageSize.getHeight()));

                // The relationship between the two pages flip flops on the back of the paper
                if (offset % 2 == 0) {
                    leftObject.setMatrix(leftTrans);
                    rightObject.setMatrix(rightTrans);
                } else {
                    leftObject.setMatrix(rightTrans);
                    rightObject.setMatrix(leftTrans);
                }

                content.drawForm(leftObject);
                content.drawForm(rightObject);
                content.close();
            }
        }

        return impositionedDoc;
    }

    private PDDocument fourUpMode(PaperSize paperSize, PaperSize pageSize, int signatures, int pagesPerSignature) throws IOException {
        PDRectangle targetPaperSize = paperSize.getRect();
        PDRectangle targetPageSize = pageSize.getRect();

        PDDocument scaledDoc = scaleDoc(pageSize, originalDoc);
        PDDocument impositionedDoc = new PDDocument();
        intermediates.add(impositionedDoc);

        final double theta = Math.PI;
        for (int signature = 0; signature < signatures; signature++) {
            for (int offset = 0; offset < pagesPerSignature / 2; offset += (offset % 2 == 0 ? 1 : 3)) {
                PDPage fourUp = new PDPage(targetPaperSize);
                fourUp.setResources(new PDResources());
                impositionedDoc.addPage(fourUp);

                int indexOffset = offset % 2 == 0 ? 3 : 1;
                int lowerLeftIndex = pagesPerSignature * signature + pagesPerSignature - offset - 1;
                int lowerRightIndex = pagesPerSignature * signature + offset;
                int upperLeftIndex = lowerLeftIndex - indexOffset;
                int upperRightIndex = lowerRightIndex + indexOffset;

                PDPage lowerLeftPage = getPage(scaledDoc, lowerLeftIndex, targetPageSize);
                PDPage lowerRightPage = getPage(scaledDoc, lowerRightIndex, targetPageSize);
                PDPage upperLeftPage = getPage(scaledDoc, upperLeftIndex, targetPageSize);
                PDPage upperRightPage = getPage(scaledDoc, upperRightIndex, targetPageSize);

                PDFormXObject lowerLeftObject = importAsXObject(impositionedDoc, lowerLeftPage);
                fourUp.getResources().add(lowerLeftObject);

                PDFormXObject lowerRightObject = importAsXObject(impositionedDoc, lowerRightPage);
                fourUp.getResources().add(lowerRightObject);

                PDFormXObject upperLeftObject = importAsXObject(impositionedDoc, upperLeftPage);
                fourUp.getResources().add(upperLeftObject);

                PDFormXObject upperRightObject = importAsXObject(impositionedDoc, upperRightPage);
                fourUp.getResources().add(upperRightObject);

                PDPageContentStream content = new PDPageContentStream(impositionedDoc, fourUp);

                AffineTransform lowerRightTrans = AffineTransform.getTranslateInstance(targetPageSize.getWidth(), 0);
                AffineTransform upperLeftTrans = AffineTransform.getRotateInstance(theta);
                upperLeftTrans.concatenate(AffineTransform.getTranslateInstance(-targetPageSize.getWidth(), -targetPageSize.getHeight() * 2));
                AffineTransform upperRightTrans = AffineTransform.getRotateInstance(theta);
                upperRightTrans.concatenate(AffineTransform.getTranslateInstance(-targetPageSize.getWidth() * 2, -targetPageSize.getHeight() * 2));

                // The relationship between the two pages flip flops on the back of the paper
                if (offset % 2 == 0) {
                    lowerRightObject.setMatrix(lowerRightTrans);
                    upperLeftObject.setMatrix(upperLeftTrans);
                    upperRightObject.setMatrix(upperRightTrans);
                } else {
                    lowerLeftObject.setMatrix(lowerRightTrans);
                    upperRightObject.setMatrix(upperLeftTrans);
                    upperLeftObject.setMatrix(upperRightTrans);
                }

                content.drawForm(lowerLeftObject);
                content.drawForm(lowerRightObject);
                content.drawForm(upperLeftObject);
                content.drawForm(upperRightObject);
                content.close();
            }
        }

        return impositionedDoc;
    }

    private PDDocument eightUpMode(PaperSize paperSize, PaperSize pageSize, int signatures, int pagesPerSignature) throws IOException {
        PaperSize twoUpPaperSize = PaperSize.values()[pageSize.ordinal() - 1];
        PDDocument twoUpDoc = twoUpMode(twoUpPaperSize, pageSize, signatures, pagesPerSignature);

        return twoUpDoc;
    }

    private PDPage getPage(PDDocument doc, int index, PDRectangle pageSize) {
        if (index < doc.getNumberOfPages()) {
            return doc.getPage(index);
        } else {
            return new PDPage(pageSize);
        }
    }

    private PDFormXObject importAsXObject(PDDocument target, PDPage page) throws IOException {
        final PDFormXObject xObject = new PDFormXObject(target);

        OutputStream os = xObject.getStream().createOutputStream();
        InputStream is = page.getContents();
        try {
            IOUtils.copy(is, os);
        } finally {
            IOUtils.closeQuietly(is);
            IOUtils.closeQuietly(os);
        }

        xObject.setResources(page.getResources());
        xObject.setBBox(page.getCropBox());

        return xObject;
    }

    public void deletePage(int index) {
        pagePreviews.remove(index);
        originalDoc.removePage(index);
    }

    public void insertPage(int index) throws IOException {
        PDPage targetPage = originalDoc.getPage(index);
        PDPage newPage = new PDPage(targetPage.getTrimBox());

        if (index >= originalDoc.getNumberOfPages()) {
            originalDoc.addPage(newPage);
        } else {
            PDPageTree rootPages = originalDoc.getDocumentCatalog().getPages();
            rootPages.insertBefore(newPage, targetPage);
        }

        PDFRenderer renderer = new PDFRenderer(originalDoc);
        Image image = convertToFxImage(renderer.renderImage(index));
        pagePreviews.add(index, image);
    }

    public void duplicatePage(int index, int times) throws IOException {
        PDPage targetPage = originalDoc.getPage(index);
        COSDictionary pageDict = targetPage.getCOSObject();
        PDFRenderer renderer = new PDFRenderer(originalDoc);

        for (int i = 0; i < times; i++) {
            COSDictionary newPageDict = new COSDictionary(pageDict);
            newPageDict.removeItem(COSName.ANNOTS);
            PDPage newPage = new PDPage(newPageDict);

            if (index >= originalDoc.getNumberOfPages()) {
                originalDoc.addPage(newPage);
            } else {
                PDPageTree rootPages = originalDoc.getDocumentCatalog().getPages();
                rootPages.insertAfter(newPage, targetPage);
            }

            Image image = convertToFxImage(renderer.renderImage(index));
            pagePreviews.add(index, image);
        }
    }

    public List<ImageView> getPreviews(double scale) {
        List<ImageView> views = new ArrayList<>(pagePreviews.size());

        for (Image page : pagePreviews) {
            ImageView preview = new ImageView(page);
            preview.setPreserveRatio(true);
            preview.setFitWidth(page.getWidth() * scale);
            preview.setFitHeight(page.getHeight() * scale);
            preview.setSmooth(true);

            views.add(preview);
        }

        return views;
    }

    private Image convertToFxImage(BufferedImage image) {
        WritableImage wr = null;
        if (image != null) {
            wr = new WritableImage(image.getWidth(), image.getHeight());
            PixelWriter pw = wr.getPixelWriter();
            for (int x = 0; x < image.getWidth(); x++) {
                for (int y = 0; y < image.getHeight(); y++) {
                    pw.setArgb(x, y, image.getRGB(x, y));
                }
            }
        }

        return new ImageView(wr).getImage();
    }
}
