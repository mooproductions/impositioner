package productions.moo;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import org.apache.pdfbox.pdmodel.common.PDRectangle;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public enum PaperSize {
    A0("A0"),
    A1("A1"),
    A2("A2"),
    A3("A3"),
    A4("A4"),
    A5("A5"),
    A6("A6"),
    A7("A7"),
    A8("A8"),
    A9("A9");

    private final String name;

    PaperSize(String paperSize) {
        this.name = paperSize;
    }

    public static ObservableList<String> getNames() {
        List<String> names = Arrays.stream(values()).map(PaperSize::toString).collect(Collectors.toList());
        return FXCollections.observableArrayList(names);
    }

    public static ObservableList<PaperSize> getSizes() {
        return FXCollections.observableArrayList(values());
    }

    public static int foldsBetween(PaperSize paper, PaperSize page) {
        return page.ordinal() - paper.ordinal();
    }

    public static int pagesPerSheet(PaperSize paper, PaperSize page) {
        return (int) Math.pow(2, foldsBetween(paper, page) + 1);
    }

    public PDRectangle getRect() {
        return switch (this) {
            case A0 -> PDRectangle.A0;
            case A1 -> PDRectangle.A1;
            case A2 -> PDRectangle.A2;
            case A3 -> PDRectangle.A3;
            case A4 -> PDRectangle.A4;
            case A5 -> PDRectangle.A5;
            case A6 -> PDRectangle.A6;
            // PDRectangle only goes up to A6
            case A7 -> {
                PDRectangle a6 = A6.getRect();
                yield new PDRectangle(a6.getHeight() / 2, a6.getWidth());
            }
            case A8 -> {
                PDRectangle a7 = A7.getRect();
                yield new PDRectangle(a7.getHeight() / 2, a7.getWidth());
            }
            case A9 -> {
                PDRectangle a8 = A8.getRect();
                yield new PDRectangle(a8.getHeight() / 2, a8.getWidth());
            }
        };
    }

    @Override
    public String toString() {
        return name;
    }
}