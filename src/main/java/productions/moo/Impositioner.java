package productions.moo;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

import java.net.URL;

public class Impositioner extends Application {
    public static final String APP_NAME = "Impositioner";
    public static final int SCENE_LOAD_FAILURE = -1;
    public static final int STYLE_LOAD_FAILURE = -2;

    public static void run(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage stage) throws Exception {
        URL sceneUrl = getClass().getResource("scene.fxml");
        if (sceneUrl == null) {
            Platform.exit();
            System.exit(SCENE_LOAD_FAILURE);
        }

        FXMLLoader loader = new FXMLLoader(sceneUrl);
        Parent root = loader.load();
        Scene scene = new Scene(root);

        URL styleUrl = getClass().getResource("styles.css");
        if (styleUrl == null) {
            Platform.exit();
            System.exit(STYLE_LOAD_FAILURE);
        }
        scene.getStylesheets().add(styleUrl.toExternalForm());

        stage.setTitle(APP_NAME);
        stage.addEventHandler(WindowEvent.WINDOW_CLOSE_REQUEST, loader.<FxmlController>getController());
        stage.setScene(scene);
        stage.show();
    }
}
